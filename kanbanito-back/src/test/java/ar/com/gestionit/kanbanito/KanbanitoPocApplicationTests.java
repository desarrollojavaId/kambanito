package ar.com.gestionit.kanbanito;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ar.com.gestionit.kanbanito.model.Tablero;
import ar.com.gestionit.kanbanito.model.TableroUsuario;
import ar.com.gestionit.kanbanito.repository.TableroRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class KanbanitoPocApplicationTests {

	@Autowired
	TableroRepository repository;
	
	@Test
	public void contextLoads() {
		Optional<Tablero> table = repository.findById(19L);
		
		TableroUsuario ta = new TableroUsuario();
		ta.setId(19L);
		repository.save(ta);
	}

}
