package ar.com.gestionit.kanbanito.web;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.gestionit.kanbanito.model.CheckItem;
import ar.com.gestionit.kanbanito.model.Comentario;
import ar.com.gestionit.kanbanito.model.Lista;
import ar.com.gestionit.kanbanito.model.Tarjeta;
import ar.com.gestionit.kanbanito.repository.CheckItemRepository;
import ar.com.gestionit.kanbanito.repository.ComentarioRepository;
import ar.com.gestionit.kanbanito.repository.TarjetaRepository;
import ar.com.gestionit.kanbanito.web.dto.Card;
import ar.com.gestionit.kanbanito.web.dto.CheckItemDto;
import ar.com.gestionit.kanbanito.web.dto.Comment;
import ar.com.gestionit.kanbanito.web.dto.HistoriaDto;

@RestController
public class CardController {
	@Autowired
	TarjetaRepository tarjetaRepository;
	
	@Autowired
	ComentarioRepository comentarioRepository;
	
	@Autowired
	CheckItemRepository checkItemRepository;

	@Autowired
	DozerBeanMapper mapper;
	
	private SimpMessagingTemplate template;

	@RequestMapping(value = "/card", method = RequestMethod.GET)
	public List<Card> getAll() {
		Iterable<Tarjeta> tarjetas = tarjetaRepository.findAll();

		List<Card> result = new ArrayList<Card>();
		for (Tarjeta tarjeta : tarjetas) {
			result.add(mapper.map(tarjeta, Card.class));
		}

		return result;
	}

	@RequestMapping(value = "/card/{id}", method = RequestMethod.GET)
	public Card get(@PathVariable("id") Long id) {
		Tarjeta tarjeta = tarjetaRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
		return mapper.map(tarjeta, Card.class);

	}

	@RequestMapping(value = "/card", method = RequestMethod.POST)
	public Card save(@RequestBody Card card) {
		Tarjeta tarjeta = mapper.map(card, Tarjeta.class);
		Tarjeta saved = tarjetaRepository.save(tarjeta);
		Card dto = mapper.map(saved, Card.class);
		// this.template.convertAndSend("/topic/tablero/"+saved.getLista().getTablero().getId(), dto);
		return dto;
	}

	@RequestMapping(value = "/card/{id}", method = RequestMethod.PUT)
	public Card update(@PathVariable("id") Long id, @RequestBody Card card) {
		Tarjeta saved = tarjetaRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
		saved.setLista(new Lista());
		mapper.map(card, saved);
		saved = tarjetaRepository.save(saved);
		return mapper.map(saved, Card.class);
	}
	
	@RequestMapping(value = "/card/{id}/history", method = RequestMethod.GET)
	public List<HistoriaDto> findHistory(@PathVariable("id") Long id) {
		return tarjetaRepository.findHistorialEstadosById(id);
	}
	
	@RequestMapping(value = "/card/{id}/comments", method = RequestMethod.GET)
	public List<Comment> findComments(@PathVariable("id") Long id) {
		List<Comentario> comentarios = tarjetaRepository.findComentariosById(id);

		List<Comment> result = new ArrayList<Comment>();
		for (Comentario cm : comentarios) {
			result.add(mapper.map(cm, Comment.class));
		}

		return result;
	}
	
	@RequestMapping(value = "/card/{id}/comments", method = RequestMethod.POST)
	public Comment save(@PathVariable("id") Long id, @RequestBody Comment comment) {
		Tarjeta tarjeta = tarjetaRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
		Comentario comentario = mapper.map(comment, Comentario.class);
		comentario.setTarjeta(tarjeta);
		Comentario saved = comentarioRepository.save(comentario);
		return mapper.map(saved, Comment.class);
	}
	
	@RequestMapping(value = "/card/{id}/checklist", method = RequestMethod.GET)
	public List<CheckItemDto> findCheckList(@PathVariable("id") Long id) {
		List<CheckItem> findAllByTarjetaId = checkItemRepository.findAllByTarjetaId(id);
		List<CheckItemDto> result = new ArrayList<CheckItemDto>();
		for (CheckItem cm : findAllByTarjetaId) {
			result.add(mapper.map(cm, CheckItemDto.class));
		}
		return result;
	}
	
	@RequestMapping(value = "/card/{id}/checklist", method = RequestMethod.POST)
	public CheckItemDto save(@PathVariable("id") Long id, @RequestBody CheckItemDto checkItemDto) {
		Tarjeta tarjeta = tarjetaRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
		CheckItem checkItem = mapper.map(checkItemDto, CheckItem.class);
		checkItem.setTarjeta(tarjeta);
		CheckItem saved = checkItemRepository.save(checkItem);
		return mapper.map(saved, CheckItemDto.class);
	}
	
	@RequestMapping(value = "/card/checklist/{id}", method = RequestMethod.PUT)
	public CheckItemDto update(@PathVariable("id") Long id, @RequestBody CheckItemDto checkItemDto) {
		CheckItem checkItem = checkItemRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
		mapper.map(checkItemDto, checkItem);
		CheckItem saved = checkItemRepository.save(checkItem);
		return mapper.map(saved, CheckItemDto.class);
	}
	
	@RequestMapping(value = "/card/checklist/tick/{id}", method = RequestMethod.PUT)
	public CheckItemDto tickItem(@PathVariable("id") Long id) {
		CheckItem checkItem = checkItemRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
		checkItem.setChecked(!checkItem.isChecked());
		CheckItem saved = checkItemRepository.save(checkItem);
		return mapper.map(saved, CheckItemDto.class);
	}
}
