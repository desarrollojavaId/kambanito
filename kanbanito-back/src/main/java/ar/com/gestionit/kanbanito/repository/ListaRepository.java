package ar.com.gestionit.kanbanito.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.kanbanito.model.Lista;

public interface ListaRepository extends PagingAndSortingRepository<Lista, Long> {
	@Query("select column from Lista as column inner join column.tablero tab where tab.id = :tablero and column.archivado = false")
	List<Lista> findByTableroId(@Param("tablero") Long id);
}
