package ar.com.gestionit.kanbanito.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class Card {
	@Getter
	@Setter
	Long id;
	@Getter
	@Setter
	String name;
	@Getter
	@Setter
	Integer order;
	@Getter
	@Setter
	Long columnId;
	
	@Getter
	@Setter
	String detail;

	@Getter
	@Setter
	String user;
	
	@Getter
	@Setter
	private int estimatedTime;
	
	@Getter
	@Setter
	private int spentTime;
	
	@Getter
	@Setter
	private String deadline;
	
	@Getter
	@Setter
	private boolean archived;
}
