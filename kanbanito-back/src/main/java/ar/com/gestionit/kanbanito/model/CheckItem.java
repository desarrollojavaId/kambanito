package ar.com.gestionit.kanbanito.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ar.com.gestionit.kanbanito.model.listener.DateUpdateListener;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "check_item")
@EntityListeners({ DateUpdateListener.class })
public class CheckItem {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	@Column(name = "descripcion", length = 500)
	private String descripcion;
	
	@Getter
	@Setter
	@Column(name = "orden")
	private Integer orden;
	
	@Getter
	@Setter
	@Column(name = "checked")
	private boolean checked = false;

	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "tarjeta_id")
	private Tarjeta tarjeta;

	@Getter
	@Setter
	@Column(name = "fecha_alta")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAlta;

	@Getter
	@Setter
	@Column(name = "fecha_modif")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModif;

	@Getter
	@Setter
	@Column
	private String usuarioModif;
}
