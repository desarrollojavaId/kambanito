package ar.com.gestionit.kanbanito.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.kanbanito.model.Tablero;
import ar.com.gestionit.kanbanito.model.Usuario;

public interface TableroRepository extends PagingAndSortingRepository<Tablero, Long> {

	@Query("select t from Tablero t where TYPE(t) = Tablero or (TYPE(t) = TableroUsuario and t.usuario = :usuario) or (TYPE(t) = TableroGrupo and :usuario in (select user from Grupo g inner join g.usuarios user where g = t.grupo))")
	Iterable<Tablero> findTableroByUsuario(@Param("usuario") Usuario usuario);

	@Query("select t from Tablero t where t.id = :id and (TYPE(t) = Tablero or (TYPE(t) = TableroUsuario and t.usuario = :usuario) or (TYPE(t) = TableroGrupo and :usuario in (select user from Grupo g inner join g.usuarios user where g = t.grupo)))")
	Optional<Tablero> getTableroByUsuarioAndId(@Param("usuario") Usuario usuario, @Param("id") Long id);

	@Transactional
	@Modifying
	@Query(value = "update TABLERO set tipo = :tipo where id = :id", nativeQuery = true)
	void cambiarTipo(@Param("id") Long id, @Param("tipo") String tipo);

}
