package ar.com.gestionit.kanbanito.web.dto;

import java.util.List;

import ar.com.gestionit.kanbanito.model.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class Board {
	@Getter
	@Setter
	Long id;
	@Getter
	@Setter
	String name;
	@Getter
	@Setter
	List<Column> columns;
	
	@Getter
	@Setter
	List<Card> cards;
	
	@Getter
	@Setter
	private boolean archived;
	
	@Getter
	@Setter
	Usuario user;
	
	@Getter
	@Setter
	Group group;
}
