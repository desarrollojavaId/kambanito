package ar.com.gestionit.kanbanito.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import ar.com.gestionit.kanbanito.model.listener.DateUpdateListener;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tarjeta")
@Audited
@EntityListeners({DateUpdateListener.class})
public class Tarjeta {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	@Column(name = "tituloTarjeta")
	private String titulo;

	@Getter
	@Setter
	@Column(name = "detalleTarjeta", columnDefinition="text")
	private String detalle;
	
	@Getter
	@Setter
	@Column(name = "colaborador", nullable=true)
	private String colaborador;
	
	@Getter
	@Setter
	@Column(name = "ordenTarjeta")
	private int orden;

	@Getter
	@Setter
	@Audited(targetAuditMode=RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne
	@JoinColumn(name = "lista_id")
	private Lista lista;
	
	@Getter
	@Setter
	@Column(nullable=true)
	private int horasEstimadas;
	
	@Getter
	@Setter
	@Column(nullable=true)
	private int horasInsumidas;
	
	@Getter
	@Setter
	@Column(nullable=true)
	private Date fechaLimite;
	
	@Getter
	@Setter
	@Column
	private boolean archivado = false;
	
	@Getter
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAlta;
	
	@Getter
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModif;
	
	@Getter
	@Setter
	@Column
	private String usuarioModif;
}
