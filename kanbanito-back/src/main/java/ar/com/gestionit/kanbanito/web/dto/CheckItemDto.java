package ar.com.gestionit.kanbanito.web.dto;

import lombok.Getter;
import lombok.Setter;

public class CheckItemDto {
	@Getter
	@Setter
	Long id;
	
	@Getter
	@Setter
	String description;
	
	@Getter
	@Setter
	Integer order;
	
	@Getter
	@Setter
	boolean checked;
	
}
