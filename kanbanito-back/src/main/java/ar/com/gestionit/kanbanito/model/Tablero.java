package ar.com.gestionit.kanbanito.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ar.com.gestionit.kanbanito.model.listener.DateUpdateListener;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Inheritance
@DiscriminatorColumn(name="tipo")
@DiscriminatorValue(Tablero.SIMPLE)
@Table(name = "tablero")
@EntityListeners({DateUpdateListener.class})
public class Tablero {
	public static final String SIMPLE = "simple";
	public static final String GRUPO = "grupo";
	public static final String USUARIO = "usuario";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	@Column(name = "descTablero")
	private String descripcion;

	@Getter
	@Setter
	@OneToMany(mappedBy = "tablero")
	private List<Lista> listas;
	
	@Getter
	@Setter
	@Column
	private boolean archivado = false;
	
	@Getter
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAlta;
	
	@Getter
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModif;
	
	@Getter
	@Setter
	@Column
	private String usuarioModif;
}
