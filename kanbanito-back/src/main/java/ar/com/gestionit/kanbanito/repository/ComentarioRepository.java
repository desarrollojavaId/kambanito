package ar.com.gestionit.kanbanito.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import ar.com.gestionit.kanbanito.model.Comentario;

public interface ComentarioRepository extends PagingAndSortingRepository<Comentario, Long> {

}
