package ar.com.gestionit.kanbanito.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import ar.com.gestionit.kanbanito.model.CheckItem;

public interface CheckItemRepository extends PagingAndSortingRepository<CheckItem, Long> {
	List<CheckItem> findAllByTarjetaId(Long id);
}
