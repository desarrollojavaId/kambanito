package ar.com.gestionit.kanbanito.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ar.com.gestionit.kanbanito.model.listener.DateUpdateListener;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lista")
@EntityListeners({DateUpdateListener.class})
public class Lista {
	
	@Getter
	@Setter
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Getter
	@Setter
	@Column(name = "descLista")
	private String descripcion;

	@Getter
	@Setter
	@Column(name = "ordenLista")
	private int orden;

	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "tablero_id")
	private Tablero tablero;
	
	@Getter
	@Setter
	@Enumerated(EnumType.STRING)
	private TipoListaEnum tipoLista;

	@Getter
	@Setter
	@OneToMany(mappedBy = "lista")
	private List<Tarjeta> tarjetas;
	
	@Getter
	@Setter
	@Column
	private boolean archivado = false;
	
	@Getter
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAlta;
	
	@Getter
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModif;
	
	@Getter
	@Setter
	@Column
	private String usuarioModif;
}
