package ar.com.gestionit.kanbanito.web.dto;

import java.util.List;
import java.util.Set;

import ar.com.gestionit.kanbanito.model.Usuario;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class Group {
	
	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	private String name;
	
	@Getter
	@Setter
    private Set<Usuario> users;
}
