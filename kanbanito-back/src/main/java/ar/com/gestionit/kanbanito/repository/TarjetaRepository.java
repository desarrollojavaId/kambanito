package ar.com.gestionit.kanbanito.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.kanbanito.model.Comentario;
import ar.com.gestionit.kanbanito.model.Tarjeta;
import ar.com.gestionit.kanbanito.web.dto.HistoriaDto;

public interface TarjetaRepository extends PagingAndSortingRepository<Tarjeta, Long> {

	@Query("select card from Tarjeta as card join card.lista lst join lst.tablero tab where tab.id = :tablero and card.archivado = false and lst.archivado = false")
	List<Tarjeta> findByTableroId(@Param("tablero") Long id);

	@Query(value = "select ta.fecha_modif as Fecha, li.desc_lista as Estado, ta.usuario_modif as Usuario from tarjeta_aud ta inner join lista li on li.id = ta.lista_id where ta.id = :tarjeta order by ta.fecha_modif desc", nativeQuery = true)
	List<HistoriaDto> findHistorialEstadosById(@Param("tarjeta") Long tarjeta);

	@Query(value = "select cm from Comentario cm inner join cm.tarjeta tar where tar.id = :tarjeta order by cm.fechaModif desc")
	List<Comentario> findComentariosById(@Param("tarjeta") Long tarjeta);

}
