package ar.com.gestionit.kanbanito.service;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class LDAPService {

	@Value("${ldap.url}")
	private String ldapURL;

	@Value("${ldap.base}")
	private String ldapBase;

	@Value("${ldap.userDn}")
	private String ldapUserDn;

	@Value("${ldap.userPwd}")
	private String ldapUserPwd;

	private String[] returnAttributes = { "sAMAccountName", "givenName", "cn", "mail" };
	
	public boolean existsUserDetails(String userName) {
		DirContext context = null;
		try {
			Hashtable<String, String> environment = new Hashtable<String, String>();
			environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			environment.put(Context.PROVIDER_URL, ldapURL);
			//environment.put(Context.SECURITY_AUTHENTICATION, "simple");
			environment.put(Context.SECURITY_PRINCIPAL, ldapUserDn);
			environment.put(Context.SECURITY_CREDENTIALS, ldapUserPwd);
			context = new InitialDirContext(environment);

			String searchBase = ldapBase;
			String FILTER = "(&((&(objectCategory=Person)(objectClass=User)))(samaccountname=" + userName + "))";
			SearchControls ctls = new SearchControls();
			ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			ctls.setReturningAttributes(returnAttributes);
			NamingEnumeration<SearchResult> answer = context.search(searchBase, FILTER, ctls);
			SearchResult result = answer.next();

			Attribute cn = result.getAttributes().get("cn");
			System.out.println("CN " + cn);

			return cn.toString().length() > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				context.close();
			} catch (NamingException e) {
			}
		}

	}

}
