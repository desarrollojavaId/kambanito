package ar.com.gestionit.kanbanito.web.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public interface HistoriaDto {
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getFecha();
	public String getEstado();
	public String getUsuario();
}
