package ar.com.gestionit.kanbanito.web.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class Column {
	@Getter
	@Setter
	Long id;
	@Getter
	@Setter
	String name;
	
	@Getter
	@Setter
	String type;
	
	@Getter
	@Setter
	Integer order;
	
	@Getter
	@Setter
	Long boardId;
	
	@Getter
	@Setter
	List<Card> cards;
	
	@Getter
	@Setter
	private boolean archived;
}
