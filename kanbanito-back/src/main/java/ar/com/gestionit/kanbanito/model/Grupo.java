package ar.com.gestionit.kanbanito.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ar.com.gestionit.kanbanito.model.listener.DateUpdateListener;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "grupo")
@EntityListeners({DateUpdateListener.class})
public class Grupo {
	
	@Getter
	@Setter
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Getter
	@Setter
	@Column(name = "nombre")
	private String nombre;
	
	@Getter
	@Setter
	@ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "grupo_usuario")
    private Set<Usuario> usuarios;
	
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAlta;
	
	@Getter
	@Setter
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModif;
	
	@Getter
	@Setter
	@Column
	private String usuarioModif;
}
