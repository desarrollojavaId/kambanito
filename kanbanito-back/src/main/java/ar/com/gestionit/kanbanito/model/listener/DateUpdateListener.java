package ar.com.gestionit.kanbanito.model.listener;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import ar.com.gestionit.utils.BeanUtils;

public class DateUpdateListener {

	private static final String fechaAlta = "fechaAlta";

	private static final String fechaModificacion = "fechaModif";

	private static final String usuarioModificacion = "usuarioModif";

	@PrePersist
	public void actualizarFechaAlta(Object entity) {
		if (BeanUtils.propertyExists(entity, fechaAlta)) {
			BeanUtils.setProperty(entity, fechaAlta, new Date());
		}
		actualizarFechaModif(entity);
	}

	@PreUpdate
	public void actualizarFechaModif(Object entity) {
		if (BeanUtils.propertyExists(entity, fechaModificacion)) {
			BeanUtils.setProperty(entity, fechaModificacion, new Date());
		}
		if (BeanUtils.propertyExists(entity, usuarioModificacion)) {
			Authentication principal = SecurityContextHolder.getContext().getAuthentication();
			if (principal != null) {
				BeanUtils.setProperty(entity, usuarioModificacion, principal.getName());
			} else {
				BeanUtils.setProperty(entity, usuarioModificacion, "nobody");
			}
		}
	}
}
