package ar.com.gestionit.kanbanito.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
@DiscriminatorValue(Tablero.GRUPO)
public class TableroGrupo extends Tablero {

	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "grupo_id")
	private Grupo grupo;
	
}
