package ar.com.gestionit.kanbanito.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.gestionit.kanbanito.model.Grupo;
import ar.com.gestionit.kanbanito.model.Lista;
import ar.com.gestionit.kanbanito.model.Tablero;
import ar.com.gestionit.kanbanito.model.TableroGrupo;
import ar.com.gestionit.kanbanito.model.TableroUsuario;
import ar.com.gestionit.kanbanito.model.Tarjeta;
import ar.com.gestionit.kanbanito.model.Usuario;
import ar.com.gestionit.kanbanito.repository.GrupoRepository;
import ar.com.gestionit.kanbanito.repository.ListaRepository;
import ar.com.gestionit.kanbanito.repository.TableroRepository;
import ar.com.gestionit.kanbanito.repository.TarjetaRepository;
import ar.com.gestionit.kanbanito.repository.UsuarioRepository;
import ar.com.gestionit.kanbanito.web.dto.Board;
import ar.com.gestionit.kanbanito.web.dto.Card;
import ar.com.gestionit.kanbanito.web.dto.Column;

@RestController
public class BoardController {

	@Autowired
	TableroRepository tableroRepository;
	
	@Autowired
	TarjetaRepository tarjetaRepository;
	
	@Autowired
	ListaRepository listaRepository;
	
	@Autowired
	GrupoRepository grupoRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	DozerBeanMapper mapper;

	@RequestMapping(value = "/tablero", method = RequestMethod.GET)
	public List<Board> getAll() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		Usuario usuario = usuarioRepository.findByUsername(username);
		Iterable<Tablero> tableros = tableroRepository.findTableroByUsuario(usuario);
		List<Board> result = new ArrayList<Board>();
		for (Tablero tablero : tableros) {
			result.add(mapper.map(tablero, Board.class));
		}

		return result;
	}
	
	@RequestMapping(value = "/tablero/{id}", method = RequestMethod.GET)
	public Board get(@PathVariable("id") Long id) {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		Usuario usuario = usuarioRepository.findByUsername(username);
		Optional<Tablero> tablero = tableroRepository.getTableroByUsuarioAndId(usuario, id);
		if (tablero.isPresent()) {
			Board board = mapper.map(tablero.get(), Board.class);
			List<Column> columns = new ArrayList<>();
			for(Lista lst : listaRepository.findByTableroId(id)) {
				columns.add(mapper.map(lst, Column.class));
			}
			board.setColumns(columns);
			List<Card> cards = new ArrayList<Card>();
			for (Tarjeta tarjeta : tarjetaRepository.findByTableroId(id)) {
				cards.add(mapper.map(tarjeta, Card.class));
			}
			board.setCards(cards);
			return board;
		}
		else
			throw new IllegalArgumentException();
	}

	@RequestMapping(value = "/tablero", method = RequestMethod.POST)
	public Board save(@RequestBody Board board) {
		Tablero tablero = mapper.map(board, Tablero.class);
		Tablero saved = tableroRepository.save(tablero);
		return mapper.map(saved, Board.class);
	}
	
	@RequestMapping(value = "/tablero/usuario", method = RequestMethod.POST)
	public Board saveByUsuario(@RequestBody Board board) {
		TableroUsuario tablero = mapper.map(board, TableroUsuario.class);
		Tablero saved = tableroRepository.save(tablero);
		return mapper.map(saved, Board.class);
	}
	
	@RequestMapping(value = "/tablero/grupo", method = RequestMethod.POST)
	public Board saveByGrupo(@RequestBody Board board) {
		TableroGrupo tablero = mapper.map(board, TableroGrupo.class);
		Tablero saved = tableroRepository.save(tablero);
		return mapper.map(saved, Board.class);
	}
	
	@RequestMapping(value = "/tablero/{id}", method = RequestMethod.PUT)
	public Board save(@PathVariable("id") Long id, @RequestBody Board board) {
		tableroRepository.cambiarTipo(id, Tablero.SIMPLE);
		Tablero tablero = tableroRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
		mapper.map(board, tablero, "tablero-simple");
		Tablero saved = tableroRepository.save(tablero);
		return mapper.map(saved, Board.class);
	}
	
	@RequestMapping(value = "/tablero/usuario/{id}", method = RequestMethod.PUT)
	public Board saveByUsuario(@PathVariable("id") Long id, @RequestBody Board board) {
		tableroRepository.cambiarTipo(id, Tablero.USUARIO);
		TableroUsuario tablero = (TableroUsuario) tableroRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
		mapper.map(board, tablero, "tablero-simple");
		Usuario usuario = usuarioRepository.findById(board.getUser().getId()).orElseThrow(() -> new IllegalArgumentException());
		tablero.setUsuario(usuario);
		Tablero saved = tableroRepository.save(tablero);
		return mapper.map(saved, Board.class);
	}
	
	@RequestMapping(value = "/tablero/grupo/{id}", method = RequestMethod.PUT)
	public Board saveByGrupo(@PathVariable("id") Long id, @RequestBody Board board) {
		tableroRepository.cambiarTipo(id, Tablero.GRUPO);
		TableroGrupo tablero = (TableroGrupo) tableroRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
		mapper.map(board, tablero, "tablero-simple");
		Grupo grupo = grupoRepository.findById(board.getGroup().getId()).orElseThrow(() -> new IllegalArgumentException());
		tablero.setGrupo(grupo);
		Tablero saved = tableroRepository.save(tablero);
		return mapper.map(saved, Board.class);
	}

}
