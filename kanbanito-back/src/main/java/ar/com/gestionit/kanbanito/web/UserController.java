package ar.com.gestionit.kanbanito.web;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.gestionit.kanbanito.model.Usuario;
import ar.com.gestionit.kanbanito.repository.UsuarioRepository;
import ar.com.gestionit.kanbanito.security.UsernameNotFoundException;
import ar.com.gestionit.kanbanito.service.LDAPService;

import javax.persistence.EntityExistsException;

@RestController
public class UserController {
	@Autowired
	LDAPService provider;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	DozerBeanMapper mapper;

	@RequestMapping(value = "/aduser/{username}", method = RequestMethod.GET)
	public boolean get(@PathVariable("username") String username) {
		return provider.existsUserDetails(username);
	}
	
	
	@RequestMapping(value = "/usuario", method = RequestMethod.GET)
	public Iterable<Usuario> getAll() {
		Iterable<Usuario> entities = usuarioRepository.findAll();
		return entities;
	}

	@RequestMapping(value = "/usuario/{id}", method = RequestMethod.GET)
	public Usuario get(@PathVariable("id") Long id) {
		return usuarioRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
	}

	@RequestMapping(value = "/usuario", method = RequestMethod.POST)
	public Usuario save(@RequestBody Usuario user) {
		if(usuarioRepository.contarUsuarios(null, user.getUsername()) > 0)
			throw new EntityExistsException("Ya existe un usuario con ese nombre de usuario");
		if(!provider.existsUserDetails(user.getUsername())) {
			throw new UsernameNotFoundException();
		}
		Usuario saved = usuarioRepository.save(user);
		return saved;
	}

	@RequestMapping(value = "/usuario/{id}", method = RequestMethod.PUT)
	public Usuario save(@PathVariable("id") Long id, @RequestBody Usuario user) {
		if(usuarioRepository.contarUsuarios(id, user.getUsername()) > 0)
			throw new EntityExistsException("Ya existe un usuario con ese nombre de usuario");
		Usuario usuario = usuarioRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("El usuario no existe"));
		mapper.map(user, usuario);
		if(!provider.existsUserDetails(usuario.getUsername())) {
			throw new UsernameNotFoundException();
		}
		Usuario saved = usuarioRepository.save(usuario);
		return saved;
	}
}
