package ar.com.gestionit.kanbanito.web;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.gestionit.kanbanito.model.Grupo;
import ar.com.gestionit.kanbanito.model.Usuario;
import ar.com.gestionit.kanbanito.repository.GrupoRepository;
import ar.com.gestionit.kanbanito.repository.UsuarioRepository;
import ar.com.gestionit.kanbanito.web.dto.Group;

import javax.persistence.EntityExistsException;

@RestController
public class GrupoController {

	@Autowired
	GrupoRepository grupoRepository;
	@Autowired
	UsuarioRepository userRepository;

	@Autowired
	DozerBeanMapper mapper;

	@RequestMapping(value = "/grupo", method = RequestMethod.GET)
	public Iterable<Group> getAll() {
		Iterable<Grupo> entities = grupoRepository.findAll();
		List<Group> result = new ArrayList<>();
		for (Grupo gr : entities) {
			result.add(mapper.map(gr, Group.class));
		}
		return result;
	}

	@RequestMapping(value = "/grupo/{id}", method = RequestMethod.GET)
	public Group get(@PathVariable("id") Long id) {
		Grupo grupo = grupoRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
		return mapper.map(grupo, Group.class, "grupo-resp");
	}

	@RequestMapping(value = "/grupo", method = RequestMethod.POST)
	public Group save(@RequestBody Group group) {
		Grupo grupo = new Grupo();
		if(grupoRepository.cantidadGrupos(null, group.getName()) > 0)
			throw new EntityExistsException("Ya existe un grupo con ese nombre");
		mapper.map(group, grupo);
		Grupo saved = grupoRepository.save(grupo);
		return mapper.map(saved, Group.class);
	}

	@RequestMapping(value = "/grupo/{id}", method = RequestMethod.PUT)
	public Group save(@PathVariable("id") Long id, @RequestBody Group group) {
		Grupo grupo = grupoRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Grupo no encontrado"));
		if(grupoRepository.cantidadGrupos(id, group.getName()) > 0)
			throw new EntityExistsException("Ya existe el grupo");
		mapper.map(group, grupo);
		grupo.getUsuarios().clear();
		if (group.getUsers() != null) {
			for (Usuario usr : group.getUsers()) {
				grupo.getUsuarios()
						.add(userRepository.findById(usr.getId()).orElseThrow(() -> new IllegalArgumentException()));
			}
		}
		Grupo saved = grupoRepository.save(grupo);

		return mapper.map(saved, Group.class);
	}
}
