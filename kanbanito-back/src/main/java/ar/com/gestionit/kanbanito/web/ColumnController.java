package ar.com.gestionit.kanbanito.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.com.gestionit.kanbanito.model.Lista;
import ar.com.gestionit.kanbanito.repository.ListaRepository;
import ar.com.gestionit.kanbanito.web.dto.Column;

@RestController
public class ColumnController {
	@Autowired
	ListaRepository listaRepository;

	@Autowired
	DozerBeanMapper mapper;

	@RequestMapping(value = "/column", method = RequestMethod.GET)
	public List<Column> getAll() {
		Iterable<Lista> listas = listaRepository.findAll();

		List<Column> result = new ArrayList<Column>();
		for (Lista lista : listas) {
			result.add(mapper.map(lista, Column.class));
		}

		return result;
	}

	@RequestMapping(value = "/column/{id}", method = RequestMethod.GET)
	public Column get(@PathVariable("id") Long id) {
		Optional<Lista> lista = listaRepository.findById(id);
		if (lista.isPresent())
			return mapper.map(lista.get(), Column.class);
		else
			throw new IllegalArgumentException();
	}

	@RequestMapping(value = "/column", method = RequestMethod.POST)
	public Column save(@RequestBody Column column) {
		Lista lista = mapper.map(column, Lista.class);
		Lista saved = listaRepository.save(lista);
		return mapper.map(saved, Column.class);
	}

	@RequestMapping(value = "/column/{id}", method = RequestMethod.PUT)
	public Column save(@PathVariable("id") Long id, @RequestBody Column column) {
		Lista lista = listaRepository.findById(id).orElseThrow(() -> new IllegalArgumentException());
		mapper.map(column, lista, "lista-simple");
		Lista saved = listaRepository.save(lista);
		return mapper.map(saved, Column.class);
	}

}
