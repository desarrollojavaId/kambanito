package ar.com.gestionit.kanbanito.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
@DiscriminatorValue(Tablero.USUARIO)
public class TableroUsuario extends Tablero {

	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;
}
