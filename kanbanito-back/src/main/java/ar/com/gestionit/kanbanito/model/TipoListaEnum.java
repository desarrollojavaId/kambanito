package ar.com.gestionit.kanbanito.model;

enum TipoListaEnum {
    BACKLOG("Backlog"), 
    PENDIENTE("Pendiente"),
    EN_PROGRESO("En Progreso"),
    FINALIZADO("Finalizado"), 
    ARCHIVADO("Archivado"),
	OTROS("Otros");
	
	private String descripcion;	
	
	TipoListaEnum(String value) {
		this.descripcion = value;
	}

	public String getDescripcion() {
		return descripcion;
	}
}
