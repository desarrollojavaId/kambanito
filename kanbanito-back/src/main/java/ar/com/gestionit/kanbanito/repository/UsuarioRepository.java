package ar.com.gestionit.kanbanito.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import ar.com.gestionit.kanbanito.model.Usuario;

public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Long> {

	Usuario findByUsername(String username);

	@Query("select count(u.id) from Usuario u where (:id is null or u.id <> :id) and u.username = :username")
	Integer contarUsuarios(Long id, String username);
}
