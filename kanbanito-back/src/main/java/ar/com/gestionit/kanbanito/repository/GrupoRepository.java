package ar.com.gestionit.kanbanito.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import ar.com.gestionit.kanbanito.model.Grupo;

public interface GrupoRepository extends PagingAndSortingRepository<Grupo, Long> {

    @Query("select count(g.id) from Grupo g where (:id is null or g.id <> :id) and g.nombre = :nombre")
    Integer cantidadGrupos(Long id, String nombre);

}
