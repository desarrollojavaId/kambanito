package ar.com.gestionit.kanbanito.web.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

public class Comment {
	@Getter
	@Setter
	Long id;
	@Getter
	@Setter
	String description;
	@Getter
	@Setter
	String user;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@Getter
	@Setter
	Date date;
}
