import {Injectable} from '@angular/core';
import {Subject, Observable, ReplaySubject, AsyncSubject, BehaviorSubject} from 'rxjs';
import {NotificationType, Notification} from './notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private subject = new BehaviorSubject<Notification>(new Notification(1, NotificationType.INFO, '', 0));
  private idx = 2;
  private seenId = 1;

  constructor() {
  }

  getObservable(): Observable<Notification> {
    return this.subject.asObservable();
  }

  getSeenId() {
    return this.seenId
  }

  setSeenId(id) {
    this.seenId = id;
  }

  info(message: string, timeout = 4000) {
    this.subject.next(new Notification(this.idx++, NotificationType.INFO, message, timeout));
  }

  success(message: string, timeout = 4000) {
    this.subject.next(new Notification(this.idx++, NotificationType.SUCCESS, message, timeout));
  }

  warning(message: string, timeout = 4000) {
    this.subject.next(new Notification(this.idx++, NotificationType.WARNING, message, timeout));
  }

  danger(message: string, timeout = 6000) {
    this.subject.next(new Notification(this.idx++, NotificationType.DANGER, message, timeout));
  }

  sinResultados() {
    this.danger('No se encontraron resultados para la búsqueda.')
  }

  sinDatos(nombre: string = 'valores', cargados: string = 'cargados') {
    console.log('cargo sin datos para', nombre)
    this.info(`No hay ${nombre} ${cargados} hasta el momento.`)
  }

}
