// DEPENDENCIES
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

// COMPONENTS
import {AlertModule} from "ngx-bootstrap";
import {NotificationDisplayComponent} from "./notification-display.component";
import {CommonModule} from "@angular/common";


@NgModule({
  declarations: [
    NotificationDisplayComponent
  ],
  imports: [
    CommonModule,
    AlertModule.forRoot()
  ],
  exports: [
    NotificationDisplayComponent
  ]
})
export class NotificationModule {
}
