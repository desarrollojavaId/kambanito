import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {NotificationService} from './notification.service';
import {Notification} from './notification';


@Component({
  selector: 'app-notification-display',
  templateUrl: './notification-display.component.html'
})
export class NotificationDisplayComponent implements OnInit, OnDestroy {

  notifications: Notification[];
  private subscription: Subscription;

  constructor(private notificationSvc: NotificationService) {
  }

  private addNotification(notification: Notification) {
    this.notifications.push(notification);
    if (notification.timeout !== 0) {
      setTimeout(() => this.close(notification), notification.timeout);
    }
  }

  ngOnInit() {
    this.notifications = [];
    this.subscription = this.notificationSvc.getObservable().subscribe(notification => {
      if (notification.id > this.notificationSvc.getSeenId()) {
        this.addNotification(notification)
        this.notificationSvc.setSeenId(notification.id)
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  close(notification: Notification) {
    this.notifications = this.notifications.filter(notif => notif.id !== notification.id);
  }
}
