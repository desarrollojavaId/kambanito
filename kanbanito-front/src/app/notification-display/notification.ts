export class Notification {
  constructor(
    public id: number,
    public type: NotificationType,
    public message: string,
    public timeout: number,
  ) {
  }
}

export enum NotificationType {
  SUCCESS = 'success',
  WARNING = 'warning',
  DANGER = 'danger',
  INFO = 'info'
}
