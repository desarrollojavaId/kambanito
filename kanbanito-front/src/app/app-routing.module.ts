import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './security/auth.guard';


const routes: Routes = [
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: '', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule ), canActivate: [AuthGuard] },
    { path: '', loadChildren: () => import('./kanban/kanban.module').then(m => m.KanbanModule ), canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
