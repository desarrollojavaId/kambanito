import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtService } from './jwt.service';
import { JwtModule, JWT_OPTIONS, JwtInterceptor } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';
import { AuthGuard } from './auth.guard';
import { HttpErrorInterceptor } from './http-error.interceptor';

export const jwtOptionsFactory = (jwtService: JwtService) => ({
  tokenGetter: () => jwtService.getAccessToken(),
  whitelistedDomains: [`${environment.apiUrl}`, 'localhost', 'localhost:8080', '172.16.1.139', '172.16.1.139:8080'],
  blacklistedRoutes: [`${environment.apiUrl}/login`]
});


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        deps: [JwtService],
        useFactory: jwtOptionsFactory
      }
    })
  ],
  providers: [JwtService,
    JwtInterceptor,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useExisting: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ]
})
export class SecurityModule { }
