import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class JwtService {

  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient, private router: Router) { }

  login(username: string, password: string) {
    return this.httpClient
        .post<{token: string}>(this.apiUrl + '/login',
        { username: username,  password: password}).pipe(tap(res => {
      this.setAccessToken(res.token);
      this.setCurrentUsername(username);
    }));
  }

  logout() {
    this.setAccessToken(null);
    this.setCurrentUsername(null);
  }

  getAccessToken() {
    return localStorage.getItem('access_token');
  }

  private setAccessToken(accessToken: string) {
    if (!accessToken) {
      localStorage.removeItem('access_token');
    } else {
      localStorage.setItem('access_token', accessToken);
    }
  }

  private setCurrentUsername(username: string) {
    if (!username) {
      localStorage.removeItem('logged_username');
    } else {
      localStorage.setItem('logged_username', username);
    }
  }
}
