import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import {Observable, noop, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {NotificationService} from "../notification-display/notification.service";



export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private notificationService: NotificationService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError((error, caught) => {
      if (error.status === 401 && error.error.message === 'Expired JWT Token') {
        return next.handle(request);
      } else {
        // intercept the respons error and displace it to the console
        console.log(error);
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
          errorMessage = `Error: ${error.message}`;
        } else {
          errorMessage = `Código: ${error.status}\nMensaje: ${error.error.message}`;
        }
        this.notificationService.danger(errorMessage, 4000);
        return of(error);
      }
    }) as any);
  }
}
