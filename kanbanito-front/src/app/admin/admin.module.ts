import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListaUsuariosComponent} from '../admin/usuarios/lista-usuarios/lista-usuarios.component';
import {EditarUsuarioComponent} from '../admin/usuarios/editar-usuario/editar-usuario.component';
import {AdminRoutingModule} from './admin-routing.module';
import {LayoutComponent} from '../admin/layout/layout.component';
import {UserService} from './user.service';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {ListaGruposComponent} from '../admin/grupos/lista-grupos/lista-grupos.component';
import {EditarGrupoComponent} from '../admin/grupos/editar-grupo/editar-grupo.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {AppModule} from "../app.module";
import {NotificationModule} from "../notification-display/notification.module";
import {AlertModule} from "ngx-bootstrap";



@NgModule({
  declarations: [
    ListaUsuariosComponent,
    EditarUsuarioComponent,
    LayoutComponent,
    ListaGruposComponent,
    EditarGrupoComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    AngularFontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NotificationModule
  ],
  providers: [
    UserService
  ]
})
export class AdminModule {
}
