import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Group } from './group';

@Injectable({
    providedIn: 'root'
})
export class GroupService {

    apiUrl = environment.apiUrl;

    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get(`${this.apiUrl}/grupo`);
    }

    get(id: string) {
        return this.http.get(`${this.apiUrl}/grupo/${id}`);
    }

    put(user: Group) {
        return this.http.put(`${this.apiUrl}/grupo/${user.id}`, user);
    }

    post(user: Group) {
        return this.http.post(`${this.apiUrl}/grupo`, user);
    }
}
