import { Component, OnInit } from '@angular/core';
import { Group } from '../../group';
import { GroupService } from '../../group.service';

@Component({
    selector: 'app-lista-grupos',
    templateUrl: './lista-grupos.component.html'
})
export class ListaGruposComponent implements OnInit {

    groups: Group[];

    constructor(private groupService: GroupService) { }

    ngOnInit() {
        this.groupService.getAll().subscribe((res: Group[]) => {
            console.log('traigo grupos');
            this.groups = res;
        });
    }

}
