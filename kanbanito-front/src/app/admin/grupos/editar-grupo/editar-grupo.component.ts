import {Component, OnInit} from '@angular/core';
import {Validators, FormBuilder} from '@angular/forms';
import {GroupService} from '../../group.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Group} from '../../group';
import {Observable} from 'rxjs';
import {UserService} from '../../user.service';

@Component({
  selector: 'app-editar-grupo',
  templateUrl: './editar-grupo.component.html'
})
export class EditarGrupoComponent implements OnInit {

  grupo: any;

  usuarios$: Observable<any>;

  grupoForm = this.fb.group({
    name: [null, Validators.required],
    users: [null, Validators.required]
  });

  constructor(private fb: FormBuilder,
              private groupService: GroupService,
              private userService: UserService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.grupo = new Group();
    this.findUsuarios();
    const id = this.route.snapshot.paramMap.get('id');
    console.log('Editandos ', id);
    if (id) {
      this.groupService.get(id).subscribe((res: Group) => {
        this.grupo = res;
        if (res.users != null) {
          this.grupo.users = res.users.map(item => item.id);
        }

        this.grupoForm.patchValue(this.grupo);
      });
    }
  }

  findUsuarios() {
    this.usuarios$ = this.userService.getAll();
  }

  guardargrupo() {
    if (this.grupoForm.valid) {
      const result: Group = Object.assign({id: this.grupo.id}, this.grupoForm.value);
      if (this.grupo.id == null) {
        this.groupService.post(result).subscribe(res => {
          this.router.navigateByUrl('/admin/grupos');
        });
      } else {
        this.groupService.put(result).subscribe(res => {
          this.router.navigateByUrl('/admin/grupos');
        });
      }
    } else {
      this.grupoForm.markAllAsTouched();
    }
  }

  get name() {
    return this.grupoForm.get('name');
  }

  get users() {
    return this.grupoForm.get('users');
  }
}
