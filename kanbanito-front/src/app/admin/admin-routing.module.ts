import { ListaUsuariosComponent } from './usuarios/lista-usuarios/lista-usuarios.component';
import { EditarUsuarioComponent } from './usuarios/editar-usuario/editar-usuario.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { ListaGruposComponent } from './grupos/lista-grupos/lista-grupos.component';
import { EditarGrupoComponent } from './grupos/editar-grupo/editar-grupo.component';

const routes: Routes = [
    {
      path: 'admin',
      component: LayoutComponent,
      children: [
          { path: 'usuarios', component: ListaUsuariosComponent },
          { path: 'usuarios/edit', component: EditarUsuarioComponent },
          { path: 'usuarios/edit/:id', component: EditarUsuarioComponent }, // , resolve: { usuarios: UsuarioResolver }

          { path: 'grupos', component: ListaGruposComponent },
          { path: 'grupos/edit', component: EditarGrupoComponent },
          { path: 'grupos/edit/:id', component: EditarGrupoComponent } // , resolve: { usuarios: UsuarioResolver }
      ]
  }];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class AdminRoutingModule { }
