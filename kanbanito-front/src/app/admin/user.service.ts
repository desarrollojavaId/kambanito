import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

    apiUrl = environment.apiUrl;

    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get(`${this.apiUrl}/usuario`);
    }

    get(id: string) {
        return this.http.get(`${this.apiUrl}/usuario/${id}`);
    }

    put(user: User) {
        return this.http.put(`${this.apiUrl}/usuario/${user.id}`, user);
    }

    post(user: User) {
        return this.http.post(`${this.apiUrl}/usuario`, user);
    }
}
