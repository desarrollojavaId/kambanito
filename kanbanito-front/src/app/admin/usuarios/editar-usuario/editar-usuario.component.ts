import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../../user';
import { switchMap } from 'rxjs/operators';


@Component({
    selector: 'app-editar-usuario',
    templateUrl: './editar-usuario.component.html',
    styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {

    usuario: any;

    usuarioForm = this.fb.group({
        username: [null, Validators.required],
        nombre: [null, Validators.required],
        mail: [null, Validators.email]
    });
    constructor(private fb: FormBuilder, private userService: UserService, private router: Router, private route: ActivatedRoute) { }

    ngOnInit() {
        this.usuario = new User();
        const id = this.route.snapshot.paramMap.get('id');
        console.log('Editandos ', id);
        if (id) {
            this.userService.get(id).subscribe((res: User) => {
                this.usuario = res;
                this.usuarioForm.patchValue(this.usuario);
            });
        }
    }

    guardarUsuario() {
        if (this.usuarioForm.valid) {
            const result: User = Object.assign({ id: this.usuario.id }, this.usuarioForm.value);
            if (this.usuario.id == null) {
                this.userService.post(result).subscribe(res => {
                    this.router.navigateByUrl('/admin/usuarios');
                });
            } else {
                this.userService.put(result).subscribe(res => {
                    this.router.navigateByUrl('/admin/usuarios');
                });
            }
        } else {
          this.usuarioForm.markAllAsTouched();
        }
    }

  get username() {
    return this.usuarioForm.get('username');
  }

  get nombre() {
    return this.usuarioForm.get('nombre');
  }

  get mail() {
    return this.usuarioForm.get('mail');
  }
}
