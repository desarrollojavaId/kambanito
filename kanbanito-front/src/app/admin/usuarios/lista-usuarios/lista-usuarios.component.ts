import { Component, OnInit } from '@angular/core';
import { User } from '../../user';
import { UserService } from '../../user.service';

@Component({
    selector: 'app-lista-usuarios',
    templateUrl: './lista-usuarios.component.html',
    styleUrls: ['./lista-usuarios.component.css']
})
export class ListaUsuariosComponent implements OnInit {

    users: User[];
    constructor(private userService: UserService) { }

    ngOnInit() {
        this.userService.getAll().subscribe((res: User[]) => {
            console.log('traigo usuarios');
            this.users = res;
        });
    }

}
