import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Column } from '../column/column';



@Injectable()
export class ColumnService {

    apiUrl = environment.apiUrl;

    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get(`${this.apiUrl}/column`);
    }

    get(id: string) {
        return this.http.get(`${this.apiUrl}/column/${id}`);
    }

    getCards(id: string) {
        return this.http.get(`${this.apiUrl}/column/${id}/cards`);
    }

    put(column: Column) {
        return this.http.put(`${this.apiUrl}/column/${column.id}`, column);
    }

    archive(column: Column) {
        const col = Object.assign({}, column, {archived: true});
        return this.http.put(`${this.apiUrl}/column/${col.id}`, col);
    }

    post(column: Column) {
        return this.http.post(`${this.apiUrl}/column`, column);
    }

    delete(column: Column) {
        return this.http.delete(`${this.apiUrl}/column/${column.id}`);
    }

}
