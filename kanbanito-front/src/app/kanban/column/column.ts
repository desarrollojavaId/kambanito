export class Column {
    id: string;
    name: string;
    type: string;
    boardId: string;
    order: number;
    archived: boolean;
}
