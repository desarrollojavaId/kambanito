import { Component, Input, Output, OnInit, AfterViewInit, EventEmitter, ElementRef } from '@angular/core';
import { Column } from './column';
import { Card } from '../card/card';
import { ColumnService } from './column.service';
import { CardService } from '../card/card.service';
import { FormBuilder, Validators } from '@angular/forms';

declare var jQuery: any;

@Component({
    selector: 'app-gtm-column',
    templateUrl: './column.component.html',
    styleUrls: ['./column.component.css'],
})
export class ColumnComponent implements OnInit {
    @Input()
    column: Column;
    @Input()
    cards: Card[];
    @Output() addedCard: EventEmitter<Card>;
    @Output() cardUpdate: EventEmitter<Card>;

    editingColumn = false;
    addCardText: string;
    addingCard = false;
    tipos = [{ id: 'BACKLOG', text: 'Backlog' },
    { id: 'PENDIENTE', text: 'Pendiente' },
    { id: 'EN_PROGRESO', text: 'En Progreso' },
    { id: 'FINALIZADO', text: 'Finalizado' },
    { id: 'OTROS', text: 'Otros' },
    { id: 'ARCHIVADO', text: 'Archivado' }
    ];

    columnForm = this.fb.group({
        name: ['', Validators.required],
        type: [null, Validators.required]
    });
    // currentTitle: string;

    constructor(private el: ElementRef,
                private fb: FormBuilder,
                private columnService: ColumnService,
                private cardService: CardService) {
        this.addedCard = new EventEmitter();
        this.cardUpdate = new EventEmitter();
    }

    ngOnInit() {
        this.setupView();
        // this._ws.onColumnUpdate.subscribe((column: Column) => {
        //     if (this.column._id === column._id) {
        //         this.column.title = column.title;
        //         this.column.order = column.order;
        //     }
        // });
    }

    setupView() {
        const component = this;
        let startColumn;
        jQuery('.card-list').sortable({
            connectWith: '.card-list',
            placeholder: 'card-placeholder',
            dropOnEmpty: true,
            tolerance: 'pointer',
            start(event, ui) {
                ui.placeholder.height(ui.item.outerHeight());
                startColumn = ui.item.parent();
            },
            stop(event, ui) {
                const senderColumnId = startColumn.attr('column-id');
                const targetColumnId = ui.item.closest('.card-list').attr('column-id');
                const cardId = ui.item.find('.card').attr('card-id');

                component.updateCardsOrder({
                    columnId: targetColumnId || senderColumnId,
                    cardId
                });
            }
        });
        jQuery('.card-list').disableSelection();
    }

    updateCardsOrder(event) {
        const cardArr = jQuery('[column-id=' + event.columnId + '] .card');
        let i = 0;
        let elBefore = -1;
        let elAfter = -1;
        let newOrder = 0;

        for (i = 0; i < cardArr.length - 1; i++) {
            if (cardArr[i].getAttribute('card-id') === event.cardId) {
                break;
            }
        }

        if (cardArr.length > 1) {
            if (i > 0 && i < cardArr.length - 1) {
                elBefore = +cardArr[i - 1].getAttribute('card-order');
                elAfter = +cardArr[i + 1].getAttribute('card-order');

                newOrder = elBefore + ((elAfter - elBefore) / 2);
            } else if (i === cardArr.length - 1) {
                elBefore = +cardArr[i - 1].getAttribute('card-order');
                newOrder = elBefore + 1000;
            } else if (i === 0) {
                elAfter = +cardArr[i + 1].getAttribute('card-order');

                newOrder = elAfter / 2;
            }
        } else {
            newOrder = 1000;
        }


        const card = this.cards.filter(x => x.id == event.cardId)[0];
        const oldColumnId = card.columnId;
        card.order = newOrder;
        card.columnId = event.columnId;
        this.cardService.put(card).subscribe((res: Card) => {
            console.log('Column Card put', res);
            this.onCardUpdate(res);
            // this._ws.updateCard(this.column.boardId, card);
        });
    }

    blurOnEnter(event) {
        if (event.keyCode === 13) {
            event.target.blur();
        }
    }

    addColumnOnEnter(event: KeyboardEvent) {
        if (event.keyCode === 13) {
            this.updateColumn();
        } else if (event.keyCode === 27) {
            this.cleadAddColumn();
        }
    }

    addCard() {
        this.cards = this.cards || [];
        const newCard = {
            name: this.addCardText,
            order: (this.cards.length + 1) * 1000,
            columnId: this.column.id,
            boardId: this.column.boardId
        } as Card;
        this.cardService.post(newCard)
            .subscribe((card: Card) => {
                this.addedCard.emit(card);
                console.log("COLUMN addCard", card);
                // this._ws.addCard(card.boardId, card);
            });
    }

    addCardOnEnter(event: KeyboardEvent) {
        if (event.keyCode === 13) {
            if (this.addCardText && this.addCardText.trim() !== '') {
                this.addCard();
                this.addCardText = '';
            } else {
                this.clearAddCard();
            }
        } else if (event.keyCode === 27) {
            this.clearAddCard();
        }
    }

    editColumn() {
        console.log(this.column);
        this.columnForm.patchValue({ name: this.column.name, type: this.column.type });
        this.editingColumn = true;
    }

    updateColumn() {
        if (this.columnForm.valid) {
            const data = Object.assign({}, this.column, this.columnForm.value);
            this.columnService.put(data).subscribe(res => {
                console.log('Put de columna', res);
                Object.assign(this.column, res);
                // this._ws.updateColumn(this.column.boardId, this.column);
            });
            this.cleadAddColumn();
        }
    }

    cleadAddColumn() {
        this.columnForm.reset();
        this.editingColumn = false;
    }

    enableAddCard() {
        this.addingCard = true;
        const input = this.el.nativeElement
            .getElementsByClassName('add-card')[0]
            .getElementsByTagName('input')[0];

        setTimeout(function () { input.focus(); }, 0);
    }


    addCardOnBlur() {
        if (this.addingCard) {
            if (this.addCardText && this.addCardText.trim() !== '') {
                this.addCard();
            }
        }
        this.clearAddCard();
    }

    clearAddCard() {
        this.addingCard = false;
        this.addCardText = '';
    }

    onCardUpdate(card: Card) {
        this.cardUpdate.emit(card);
    }

    archive(column: Column) {
        if (confirm('Confirme para archivar la Lista')) {
            this.columnService.archive(this.column).subscribe((res: Column) => {
                console.log('Archive de columna', res);
                this.column.archived = true;
                this.editingColumn = false;
                // this._ws.updateColumn(this.column.boardId, this.column);
            });
        }
    }
}
