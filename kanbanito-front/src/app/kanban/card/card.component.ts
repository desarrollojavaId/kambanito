import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ChangeDetectorRef, NgZone, ViewContainerRef } from '@angular/core';
import { Card } from './card';
import { CardService } from './card.service';
import { EditDialogComponent } from '../edit-dialog/edit-dialog.component';
import { Subject } from 'rxjs';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Component({
    selector: 'app-gtm-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
    @Input()
    card: Card;
    @Output() cardUpdate: EventEmitter<Card>;
    editingCard = false;
    currentTitle: string;
    zone: NgZone;

    bsModalRef: BsModalRef;
    editCardModalEvent: EventEmitter<Card> = new EventEmitter<Card>();
    archiveCardModalEvent: EventEmitter<Card> = new EventEmitter<Card>();

    constructor(private el: ElementRef, private ref: ChangeDetectorRef,
                private cardService: CardService,
                private modalService: BsModalService,
                private viewRef: ViewContainerRef) {
        this.zone = new NgZone({ enableLongStackTrace: false });
        this.cardUpdate = new EventEmitter();
    }

    ngOnInit() {
        if (this.editCardModalEvent) {
            // listens for changes on the Dialog
            this.editCardModalEvent.subscribe((someData) => {
                this.cardService.put(someData).subscribe((res: Card) => {
                    this.onCardUpdate(res);
                });

            });
        }
        if (this.archiveCardModalEvent) {
            this.archiveCardModalEvent.subscribe((someData) => {
                console.log('Paso 2', someData);
                this.cardService.archive(someData).subscribe((res: Card) => {
                    console.log('Paso 3', res);
                    this.onCardUpdate(res);
                });

            });
        }
    }

    blurOnEnter(event) {
        if (event.keyCode === 13) {
            event.target.blur();
        } else if (event.keyCode === 27) {
            this.card.name = this.currentTitle;
            this.editingCard = false;
        }
    }

    editCard(tarjeta) {
        this.modalService.show(EditDialogComponent, {
            class: 'modal-lg',
            ignoreBackdropClick: true,
            initialState: {
            card: tarjeta,
            saveEvent: this.editCardModalEvent,
            archiveEvent: this.archiveCardModalEvent
        }});
    }

    updateCard() {
        if (!this.card.name || this.card.name.trim() === '') {
            this.card.name = this.currentTitle;
        }

        this.cardService.put(this.card).subscribe((res: Card) => {
            console.log('no anda pna');
            this.onCardUpdate(res);
            // this._ws.updateCard(this.card.boardId, this.card);
        });
        this.editingCard = false;
    }

    onCardUpdate(card: Card) {
        this.cardUpdate.emit(card);
    }

    editar($event) {
        alert('chiooo');
        return true;
    }
}
