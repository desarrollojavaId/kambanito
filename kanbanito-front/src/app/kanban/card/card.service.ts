import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Card } from './card';

@Injectable()
export class CardService {

    apiUrl = environment.apiUrl;

    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get(`${this.apiUrl}/card`);
    }

    get(id: string) {
        return this.http.get(`${this.apiUrl}/card/${id}`);
    }

    put(card: Card) {
        return this.http.put(`${this.apiUrl}/card/${card.id}`, card);
    }

    post(card: Card) {
        return this.http.post(`${this.apiUrl}/card`, card);
    }

    delete(card: Card) {
        return this.http.delete(`${this.apiUrl}/card/${card.id}`);
    }

    archive(card: Card) {
        card.archived = true;
        return this.http.put(`${this.apiUrl}/card/${card.id}`, card);
    }

    findHistorial(id: string) {
        return this.http.get(`${this.apiUrl}/card/${id}/history`);
    }

    findComentarios(id: string) {
        return this.http.get(`${this.apiUrl}/card/${id}/comments`);
    }

    postComentario(id: string, comment: any) {
        return this.http.post(`${this.apiUrl}/card/${id}/comments`, comment);
    }

    findChecklist(id: string) {
        return this.http.get(`${this.apiUrl}/card/${id}/checklist`);
    }

    postCheckItem(id: string, description: string) {
        return this.http.post(`${this.apiUrl}/card/${id}/checklist`, {description});
    }

    putCheckItem(item: any) {
        return this.http.put(`${this.apiUrl}/card/checklist/${item.id}`, item);
    }
    tickItem(id: any) {
        return this.http.put(`${this.apiUrl}/card/checklist/tick/${id}`, {});
    }
}
