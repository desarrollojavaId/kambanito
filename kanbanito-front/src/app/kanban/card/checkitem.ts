export class Card {
    id: string;
    description: string;
    card: any;
    order: number;
}
