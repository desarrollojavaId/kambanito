export class Card {
    id: string;
    name: string;
    columnId: string;
    boardId: string;
    order: number;
    detail: string;
    user: string;
    estimatedTime: number;
    spentTime: number;
    deadline: Date;
    archived: boolean;
}
