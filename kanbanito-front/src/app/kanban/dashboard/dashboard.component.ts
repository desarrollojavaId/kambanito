import { OnInit, Component, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BoardService } from '../board/board.service';
import { Board } from '../board/board';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/admin/user.service';
import { GroupService } from 'src/app/admin/group.service';
import { group } from '@angular/animations';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
    boards: any[];
    grupos: any[];
    usuarios: any[];
    addingBoard = false;

    boardForm = this.fb.group({
        name: ['', Validators.required],
        tipo: ['', Validators.required],
        user: null,
        group: null
    });

    constructor(private boardService: BoardService,
                private groupService: GroupService,
                private userService: UserService,
                private router: Router,
                private fb: FormBuilder) { }

    ngOnInit() {
        this.boards = [];
        this.reload();
        this.groupService.getAll().subscribe((groups: any[]) => {
            this.grupos = groups;
        });
        this.userService.getAll().subscribe((users: any[]) => {
            this.usuarios = users;
        });
        this.boardForm.get('tipo').valueChanges.subscribe(val => {
            this.boardForm.patchValue({ usuario: null, grupo: null });
        });
    }

    public reload() {
        this.boardService.getAll().subscribe((result: any[]) => {
            this.boards = result;
        });
    }

    clearAddBoard() {
        this.boardForm.reset();
        this.addingBoard = false;
    }

    enableAddBoard() {
        this.addingBoard = true;
    }

    addBoard() {
        if (this.boardForm.valid) {
            let newBoard = new Board();
            newBoard = Object.assign(newBoard, this.boardForm.value);
            if (this.boardForm.value.tipo == 'usuario') {
                newBoard = Object.assign(newBoard, { user: { id: this.boardForm.value.user }, group: null });

            }
            if (this.boardForm.value.tipo == 'grupo') {
                newBoard = Object.assign(newBoard, { group: { id: this.boardForm.value.group }, user: null });
            }
            this.boardService.post(newBoard).subscribe(() => {
                this.clearAddBoard();
                this.reload();
            });
        }
    }

    findIndexToUpdate(newItem) {
        return newItem.id == this;
    }

}
