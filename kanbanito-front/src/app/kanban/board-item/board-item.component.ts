import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Board } from '../board/board';
import { BoardService } from '../board/board.service';
import { GroupService } from 'src/app/admin/group.service';
import { UserService } from 'src/app/admin/user.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-board-item',
    templateUrl: './board-item.component.html'
})
export class BoardItemComponent implements OnInit {

    grupos: any[];
    usuarios: any[];
    updateBoard = false;
    editedBoard: Board;
    @Input() board: Board;
    @Output() boardUpdate: EventEmitter<any>;

    boardForm = this.fb.group({
        name: ['', Validators.required],
        tipo: ['', Validators.required],
        user: null,
        group: null
    });

    constructor(private boardService: BoardService,
                private groupService: GroupService,
                private userService: UserService,
                private fb: FormBuilder) {
        this.boardUpdate = new EventEmitter();
    }

    ngOnInit() {
        this.groupService.getAll().subscribe((groups: any[]) => {
            this.grupos = groups;
        });
        this.userService.getAll().subscribe((users: any[]) => {
            this.usuarios = users;
        });
        this.boardForm.get('tipo').valueChanges.subscribe(val => {
            this.boardForm.patchValue({ usuario: null, grupo: null });
        });
    }

    archive(board: Board) {
        if (confirm('Confirme para archivar el Tablero')) {
            this.boardService.archive(board).subscribe((res: Board) => {
                console.log('Archive de columna', res);
                this.boardUpdate.emit();
            });
        }
        return false;
    }

    editBoard(board: Board) {
        this.editedBoard = board;
        this.boardForm.patchValue(board);
        console.log(board);
        if (board.user != null) {
            this.boardForm.patchValue({ tipo: 'usuario', user: board.user.id });
        } else if (board.group != null) {
            this.boardForm.patchValue({ tipo: 'grupo', group: board.group.id });
        } else {
            this.boardForm.patchValue({ tipo: 'general' });
        }
        this.updateBoard = true;
        return false;
    }

    clearEditBoard() {
        this.updateBoard = false;
        this.boardForm.reset();
    }

    saveBoard() {
        if (this.boardForm.valid) {
            let newBoard = Object.assign(this.editedBoard, this.boardForm.value);
            if (this.boardForm.value.tipo == 'usuario') {
                newBoard = Object.assign(newBoard, { user: { id: this.boardForm.value.user }, group: null });
            } else if (this.boardForm.value.tipo == 'grupo') {
                newBoard = Object.assign(newBoard, { group: { id: this.boardForm.value.group }, user: null });
            } else {
                newBoard = Object.assign(newBoard, { group: null, user: null });
            }
            this.boardService.put(newBoard).subscribe(() => {
                this.clearEditBoard();
                this.boardUpdate.emit();
            });
        }
    }

}
