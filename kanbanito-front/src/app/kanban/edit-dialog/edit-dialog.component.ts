import { Component, ComponentRef, EventEmitter, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Card } from '../card/card';
import * as moment from 'moment';
import { UserService } from 'src/app/admin/user.service';
import { BsModalRef } from 'ngx-bootstrap';
import { CardService } from '../card/card.service';

@Component({
    selector: 'app-edit-dialog',
    templateUrl: './edit-dialog.component.html',
    styleUrls: ['./edit-dialog.component.css'],
})
export class EditDialogComponent implements OnInit {
    tarjetaForm = this.fb.group({
        name: ['', Validators.required],
        detail: [''],
        user: null,
        estimatedTime: [''],
        spentTime: [''],
        deadline: null,
    });

    comentarioForm = this.fb.group({
        description: ['', Validators.required]
    });

    card: Card;
    usuarios: any[];
    saveEvent: EventEmitter<any>;
    archiveEvent: EventEmitter<any>;

    historial: any[];

    comentarios: any[];

    checkitems: any[];

    constructor(public bsModalRef: BsModalRef,
                private fb: FormBuilder,
                private cardService: CardService,
                private usuarioService: UserService) {
        this.usuarioService.getAll().subscribe((res: any[]) => {
            this.usuarios = res;
        });
    }

    ngOnInit() {
        // no processing needed
        console.log(this.card);
        this.tarjetaForm.patchValue(this.card);
        if (this.card.deadline != null) {
            console.log(moment(this.card.deadline, 'DD/MM/YYYY'));
            this.tarjetaForm.patchValue({ deadline: moment(this.card.deadline, 'DD/MM/YYYY').toDate() });
        }
        this.cardService.findHistorial(this.card.id).subscribe((result: any[]) => {
            console.log(result);
            this.historial = result;
        });

        this.cargarComentarios();
        this.cargarChecklist();
    }

    cargarComentarios() {
        this.cardService.findComentarios(this.card.id).subscribe((result: any[]) => {
            console.log(result);
            this.comentarios = result;
        });
    }

    cargarChecklist() {
        this.cardService.findChecklist(this.card.id).subscribe((result: any[]) => {
            console.log(result);
            this.checkitems = result;
        });
    }

    getRandomColor() {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    guardarTarjeta() {
        if (this.tarjetaForm.valid) {
            let card = Object.assign(this.card, this.tarjetaForm.value);
            if (this.tarjetaForm.value.deadline != null) {
                card = Object.assign(card, { deadline: moment(this.tarjetaForm.value.deadline).format('DD/MM/YYYY') });
            }
            this.saveEvent.emit(card);
            this.tarjetaForm.reset();
            this.bsModalRef.hide();
        }
    }

    cancelar() {
        this.tarjetaForm.reset();
        this.bsModalRef.hide();
    }

    guardarComentario() {
        if (this.comentarioForm.valid) {
            this.cardService.postComentario(this.card.id, this.comentarioForm.value).subscribe(() => {
                this.comentarioForm.reset();
                this.cargarComentarios();
            });
        }
    }

    archivar() {
        if (confirm('Confirme para archivar la Tarjeta')) {
            this.archiveEvent.emit(this.card);
        }
        return true;
    }


    crearCheckitem(texto: string) {
        this.cardService.postCheckItem(this.card.id, texto).subscribe(res => {
            this.cargarChecklist();
        });
    }

    clickCheckItem(item) {
        this.cardService.tickItem(item.id).subscribe(res => {});
    }

    actualizarCheckitem(item, texto: string) {
        item.description = texto;
        this.cardService.putCheckItem(item).subscribe(res => {
            this.cargarChecklist();
        });
    }
}
