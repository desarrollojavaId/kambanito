import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from '../kanban/layout/layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InlineEditComponent } from './inline-edit/inline-edit.component';
import { BoardComponent } from './board/board.component';
import { ColumnComponent } from './column/column.component';
import { CardComponent } from './card/card.component';
import { EditDialogComponent } from './edit-dialog/edit-dialog.component';
import { KanbanRoutingModule } from './kanban-routing.module';
import { BoardService } from './board/board.service';
import { CardService } from './card/card.service';
import { ColumnService } from './column/column.service';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { OrderBy } from './pipes/orderby.pipe';
import { Where } from './pipes/where.pipe';
import { BsDatepickerModule, BsDropdownModule, ModalModule, TabsModule } from 'ngx-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BoardItemComponent } from '../kanban/board-item/board-item.component';



@NgModule({
    declarations: [
        LayoutComponent,
        OrderBy,
        Where,
        DashboardComponent,
        InlineEditComponent,
        BoardComponent,
        ColumnComponent,
        CardComponent,
        EditDialogComponent,
        BoardItemComponent,
    ],
    providers: [
        BoardService, CardService, ColumnService
    ],
    imports: [
        CommonModule,
        KanbanRoutingModule,
        AngularFontAwesomeModule,
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        TabsModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule
    ],
    entryComponents: [
        EditDialogComponent
    ]
})
export class KanbanModule { }
