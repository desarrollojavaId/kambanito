import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';

export class WebSocketAPI {
    webSocketEndPoint = 'http://localhost:8080/ws';
    topic = '/topic/greetings';
    stompClient: any;

    constructor() {
    }
    _connect() {
        console.log('Initialize WebSocket Connection');
        let ws = new SockJS(this.webSocketEndPoint);
        this.stompClient = Stomp.over(ws);
        const _this = this;
        _this.stompClient.connect({}, function (frame) {
            _this.stompClient.subscribe(_this.topic, function (sdkEvent) {
                _this.onMessageReceived(sdkEvent);
            });
        }, this.errorCallBack);
    };

    _disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        console.log('Disconnected');
    }


    errorCallBack(error) {
        console.log('errorCallBack -> ' + error)
        setTimeout(() => {
            this._connect();
        }, 5000);
    }


    _send(message) {
        console.log('calling logout api via web socket');
        this.stompClient.send('/app/hello', {}, JSON.stringify(message));
    }

    onMessageReceived(message) {
        console.log('Message Recieved from Server :: ' + message);
    }
}
