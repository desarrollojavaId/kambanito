import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Board } from '../board/board';
import { BoardService } from './board.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Column } from '../column/column';
import { Card } from '../card/card';
import { ColumnService } from '../column/column.service';
import { FormBuilder, Validators } from '@angular/forms';
import { WebSocketAPI } from '../websocket';

declare var jQuery: any;
let curYPos = 0;
let curXPos = 0;
let curDown = false;

@Component({
    selector: 'app-gtm-board',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit, OnDestroy {
    board: Board;
    addingColumn = false;
    tipos = [{ id: 'BACKLOG', text: 'Backlog' },
    { id: 'PENDIENTE', text: 'Pendiente' },
    { id: 'EN_PROGRESO', text: 'En Progreso' },
    { id: 'FINALIZADO', text: 'Finalizado' },
    { id: 'OTROS', text: 'Otros' },
    { id: 'ARCHIVADO', text: 'Archivado' }
    ];


    columnForm = this.fb.group({
        title: ['', Validators.required],
        type: [null, Validators.required]
    });
    editingTilte = false;
    currentTitle: string;
    boardWidth: number;
    columnsAdded: 0;

    constructor(public el: ElementRef,
                private boardService: BoardService,
                private columnService: ColumnService,
                private route: ActivatedRoute,
                private fb: FormBuilder) {
    }

    ngOnInit() {
        const boardId = this.route.snapshot.paramMap.get('id');
        this.boardService.get(boardId)
            .subscribe((data: Board) => {
                console.log(`joining board ${boardId}`);
                // this._ws.join(boardId);

                this.board = data;
                console.log('BOARD ', this.board.cards);
                // this.board.columns = data[1];
                // this.board.cards = data[2];
                document.title = this.board.name + ' | Generic Task Manager';
                this.setupView();
            });
    }

    ngOnDestroy() {
    }

    setupView() {
        const component = this;
        setTimeout(function () {
            let startColumn;
            jQuery('#main').sortable({
                items: '.sortable-column',
                handler: '.header',
                connectWith: '#main',
                placeholder: 'column-placeholder',
                dropOnEmpty: true,
                tolerance: 'pointer',
                start(event, ui) {
                    ui.placeholder.height(ui.item.find('.column').outerHeight());
                    startColumn = ui.item.parent();
                },
                stop(event, ui) {
                    const columnId = ui.item.find('.column').attr('column-id');

                    component.updateColumnOrder({
                        columnId
                    });
                }
            }).disableSelection();

            // component.bindPane();;

            window.addEventListener('resize', function (e) {
                component.updateBoardWidth();
            });
            component.updateBoardWidth();
            document.getElementById('content-wrapper').style.backgroundColor = '';
        }, 100);
    }

    bindPane() {
        const el = document.getElementById('content-wrapper');
        el.addEventListener('mousemove', function (e) {
            e.preventDefault();
            if (curDown === true) {
                el.scrollLeft += (curXPos - e.pageX) * .25; // x > 0 ? x : 0;
                el.scrollTop += (curYPos - e.pageY) * .25; // y > 0 ? y : 0;
            }
        });

        el.addEventListener('mousedown', function (e) {
            const source = e.srcElement as HTMLElement;
            if (source.id === 'main' || source.id === 'content-wrapper') {
                curDown = true;
            }
            curYPos = e.pageY; curXPos = e.pageX;
        });
        el.addEventListener('mouseup', function (e) {
            curDown = false;
        });
    }

    updateBoardWidth() {
        // this.boardWidth = ((this.board.columns.length + (this.columnsAdded > 0 ? 1 : 2)) * 280) + 10;
        this.boardWidth = ((this.board.columns.length + 1) * 280) + 10;

        if (this.boardWidth > document.body.scrollWidth) {
            document.getElementById('main').style.width = this.boardWidth + 'px';
        } else {
            document.getElementById('main').style.width = '100%';
        }

        if (this.columnsAdded > 0) {
            const wrapper = document.getElementById('content-wrapper');
            wrapper.scrollLeft = wrapper.scrollWidth;
        }

        this.columnsAdded++;
    }

    updateBoard() {
        if (this.board.name && this.board.name.trim() !== '') {
            this.boardService.put(this.board);
        } else {
            this.board.name = this.currentTitle;
        }
        this.editingTilte = false;
        document.title = this.board.name + ' | Generic Task Manager';
    }

    editTitle() {
        this.currentTitle = this.board.name;
        this.editingTilte = true;

        const input = this.el.nativeElement
            .getElementsByClassName('board-title')[0]
            .getElementsByTagName('input')[0];

        setTimeout(function () { input.focus(); }, 0);
    }

    updateColumnElements(column: Column) {
        const columnArr = jQuery('#main .column');
        const columnEl = jQuery('#main .column[columnid=' + column.id + ']');
        let i = 0;
        for (; i < columnArr.length - 1; i++) {
            column.order < +columnArr[i].getAttibute('column-order');
            break;
        }

        columnEl.remove().insertBefore(columnArr[i]);
    }

    updateColumnOrder(event) {
        let i = 0;
        let elBefore = -1;
        let elAfter = -1;
        let newOrder = 0;
        const columnEl = jQuery('#main');
        const columnArr = columnEl.find('.column');

        for (i = 0; i < columnArr.length - 1; i++) {
            if (columnEl.find('.column')[i].getAttribute('column-id') === event.columnId) {
                break;
            }
        }

        if (i > 0 && i < columnArr.length - 1) {
            elBefore = +columnArr[i - 1].getAttribute('column-order');
            elAfter = +columnArr[i + 1].getAttribute('column-order');

            newOrder = elBefore + ((elAfter - elBefore) / 2);
        } else if (i === columnArr.length - 1) {
            elBefore = +columnArr[i - 1].getAttribute('column-order');
            newOrder = elBefore + 1000;
        } else if (i === 0) {
            elAfter = +columnArr[i + 1].getAttribute('column-order');
            newOrder = elAfter / 2;
        }

        const column = this.board.columns.filter(x => x.id == event.columnId)[0];
        column.order = newOrder;
        this.columnService.put(column).subscribe(res => {
            console.log('Actualizando ', column);
        });
    }


    blurOnEnter(event) {
        if (event.keyCode === 13) {
            event.target.blur();
        }
    }

    enableAddColumn() {
        this.addingColumn = true;
        const input = jQuery('.add-column')[0]
            .getElementsByTagName('input')[0];

        setTimeout(function () { input.focus(); }, 0);
    }

    addColumn() {
        if (this.columnForm.valid) {
            const newColumn = {
                name: this.columnForm.value.title,
                type: this.columnForm.value.type,
                order: (this.board.columns.length + 1) * 1000,
                boardId: this.board.id
            } as Column;
            this.columnService.post(newColumn)
                .subscribe(column => {
                    this.board.columns.push(column);
                    console.log('column added');
                    this.updateBoardWidth();
                    this.addingColumn = false;
                    this.columnForm.reset();
                });
        }

    }

    clearAddColumn() {
        this.addingColumn = false;
        this.columnForm.reset();
        //     this.addColumnText = '';
    }


    addCard(card: Card) {
        this.board.cards.push(card);
        console.log('BOARD addCard ', this.board.cards);
    }

    onCardUpdate(card: Card) {
        const updateItem = this.board.cards.find(this.findIndexToUpdate, card.id);
        const index = this.board.cards.indexOf(updateItem);
        this.board.cards[index] = card;
        console.log('BOARD onCardUpdate ', this.board.cards);
    }

    onArchived(card: Card) {
        const updateItem = this.board.cards.find(this.findIndexToUpdate, card.id);
        const index = this.board.cards.indexOf(updateItem);
        this.board.cards.splice(index, 1);
        console.log('BOARD onArchived ', this.board.cards);
    }

    findIndexToUpdate(newItem) {
        return newItem.id == this;
    }
}
