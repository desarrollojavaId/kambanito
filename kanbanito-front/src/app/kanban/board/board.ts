export class Board {
    id: string;
    name: string;
    columns: any[];
    cards: any[];
    archived: boolean;
    group: any;
    user: any;
}
