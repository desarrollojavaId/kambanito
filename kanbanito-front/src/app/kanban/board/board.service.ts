import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Board } from './board';


@Injectable({
    providedIn: 'root'
})
export class BoardService {

    apiUrl = environment.apiUrl;

    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get(`${this.apiUrl}/tablero`);
    }

    get(id: string) {
        return this.http.get(`${this.apiUrl}/tablero/${id}`);
    }

    post(board: Board) {
        if (board.user != null && board.user.id != null) {
            console.log('usuario');
            return this.http.post(`${this.apiUrl}/tablero/usuario`, board);
        } else if (board.group != null && board.group.id != null) {
            console.log('grupo');
            return this.http.post(`${this.apiUrl}/tablero/grupo`, board);
        } else {
            console.log('comun');
            return this.http.post(`${this.apiUrl}/tablero`, board);
        }
    }

    put(board: Board) {
        if (board.user != null && board.user.id != null) {
            console.log('usuario');
            return this.http.put(`${this.apiUrl}/tablero/usuario/${board.id}`, board);
        } else if (board.group != null && board.group.id != null) {
            console.log('grupo');
            return this.http.put(`${this.apiUrl}/tablero/grupo/${board.id}`, board);
        } else {
            console.log('comun');
            return this.http.put(`${this.apiUrl}/tablero/${board.id}`, board);
        }
    }

    archive(board: Board) {
        const col = Object.assign({}, board, { archived: true });
        return this.http.put(`${this.apiUrl}/tablero/${board.id}`, col);
    }
}
