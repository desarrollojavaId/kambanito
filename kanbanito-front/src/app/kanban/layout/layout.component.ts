import { Component, OnInit } from '@angular/core';
import { JwtService } from 'src/app/security/jwt.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

    showDisclaimer = true;
    constructor(private jwtService: JwtService, private router: Router) {
    }

    ngOnInit() {
        if (localStorage.disclaimerClosed === 'true') {
            this.showDisclaimer = false;
        } else {
            this.showDisclaimer = true;
        }
    }

    closeDisclaimer() {
        this.showDisclaimer = false;
        localStorage.disclaimerClosed = true;
    }

    logout() {
        this.jwtService.logout();
        this.router.navigateByUrl('login');
    }

    isLogged() {
        return localStorage.getItem('access_token') !== null;
    }

}
