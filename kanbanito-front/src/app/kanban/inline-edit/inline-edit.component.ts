import { Component, Input, Output, EventEmitter } from '@angular/core';


@Component({
    selector: 'app-inline-edit',
    templateUrl: './inline-edit.component.html',
    styleUrls: ['./inline-edit.component.css']
})
export class InlineEditComponent {
    isDisplay = true;

    @Input() text: string;
    @Output() edit = new EventEmitter<string>();

    beginEdit(el: HTMLInputElement): void {
        this.isDisplay = false;

        setTimeout(() => {
            el.value = '';
            el.focus();
        }, 100);
    }

    editDone(editText: any): void {
        this.isDisplay = true;
        // this.text = newText;
        this.edit.emit(editText.value);
        editText.value = '';
    }

    editExit() {
        this.isDisplay = true;
    }

}
