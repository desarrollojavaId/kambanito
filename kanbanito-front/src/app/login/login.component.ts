import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { JwtService } from '../security/jwt.service';
import { Router } from '@angular/router';
import {NotificationService} from "../notification-display/notification.service";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    loginForm = this.fb.group({
        usuario: [null, Validators.required],
        password: [null, Validators.required]
    });
    constructor(private fb: FormBuilder, private jwtService: JwtService, private router: Router, private notificationService: NotificationService) { }

    ngOnInit() {
    }

    onSubmit() {
        if (this.loginForm.valid) {
            this.jwtService.login(this.loginForm.value.usuario, this.loginForm.value.password).subscribe(data => {
                console.log('Ingreso correctamente');
                this.router.navigateByUrl('/dashboard');
              }, err => {
                this.notificationService.warning("Usuario o contraseña incorrecta", 3000);
              });
        } else {
          this.loginForm.markAllAsTouched();
        }
    }

    get usuario(){
      return this.loginForm.get("usuario");
    }

  get password(){
    return this.loginForm.get("password");
  }
}
